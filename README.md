```
# 1. clone repo
git clone https://amokrushin@bitbucket.org/amokrushin/hello-world-vue-nodejs-docker.git
cd hello-world-vue-nodejs-docker

# 2. create env configuration file
cp example.env .env

# 3. install dependencies
cd modules/web-server/
npm install
cd ../..

# 4. start demo-database
docker-compose up -d demo-database

# 5. start web-server
docker-compose up -d web-server

# 6. check that services are successfully started
docker-compose ps

# 7. open http://[WEB_SERVER_PUBLIC_IP]:[WEB_SERVER_PUBLIC_PORT] in browser
# example http://localhost:8080
```