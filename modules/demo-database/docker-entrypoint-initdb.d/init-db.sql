CREATE TABLE students (
    id SERIAL PRIMARY KEY,
	student JSONB NOT NULL
);

INSERT INTO students (student) VALUES
('{"name": {"first_name": "Brandon"}}');
