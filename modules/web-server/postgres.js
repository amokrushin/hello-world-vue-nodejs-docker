const { Pool } = require('pg');

const {
    POSTGRES_HOST = 'localhost',
    POSTGRES_PORT = 5432,
    POSTGRES_USER,
    POSTGRES_PASSWORD,
    POSTGRES_DB,
} = process.env;

const config = {
    host: POSTGRES_HOST,
    port: POSTGRES_PORT,
    user: POSTGRES_USER,
    password: POSTGRES_PASSWORD,
    database: POSTGRES_DB,
    // max number of clients in the pool
    max: 10,
    // how long a client is allowed to remain idle before being closed
    idleTimeoutMillis: 100,
    // abort any statement that takes more than the specified number of milliseconds
    statement_timeout: 1000,
};

const pool = new Pool(config);

pool.on('error', (err) => {
    console.error('[POSTGRES]', 'idle client error', err.message, err.stack);
    throw err;
});

pool.connect((err, client) => {
    if (err) {
        console.error('[POSTGRES]', 'error', err.message, err.stack);
        throw err;
    } else {
        console.info('[POSTGRES]', 'connected');
        client.release();
    }
});

module.exports = pool;
