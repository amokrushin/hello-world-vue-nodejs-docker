require('dotenv').config();
const express = require('express');
const http = require('http');
const WebSocket = require('ws');
const client = require('./postgres');

const app = express();
const server = http.createServer(app);
const wss = new WebSocket.Server({ server });

app.use(express.static('public'));

wss.on('connection', function connection(ws) {
    ws.on('message', (message) => {
        console.log('[WebSocket]', 'received', message);
        client.query(`UPDATE students SET student=jsonb_set(student,'{name,first_name}','"${message}"',true) WHERE id=1`);
    });

    client.query(`SELECT student -> 'name' ->> 'first_name' AS name FROM students`).then((res) => {
        if (res.rows.length) {
            const message = res.rows[0].name;
            console.log('[WebSocket]', 'send', message);
            ws.send(message);
        }
    });
});

server.listen(80, function listening() {
    console.log('Listening on %d', server.address().port);
});
